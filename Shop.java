import java.util.*;
public class Shop {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Banana[] banana = new Banana[4];
		
		for (int i=0; i < 4; i++) {
			System.out.println("--Banana #" + (i+1) + "--");
			banana[i] = new Banana();
			
			System.out.println("Banana Length: ");
			banana[i].length = scan.nextInt();
			
			System.out.println("Banana Color: ");
			banana[i].color = scan.next();
			
			System.out.println("Banana Texture: ");
			banana[i].texture = scan.next();
		}
		
		System.out.println(banana[3].length);
		System.out.println(banana[3].color);
		System.out.println(banana[3].texture);
		
		Banana ba = new Banana();
		ba.printInformation(banana[3].texture);
	}
}